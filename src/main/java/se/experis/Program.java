package se.experis;
import java.sql.*;
import java.util.ArrayList;
import java.util.Scanner;

public class Program {


    public static void main(String[] args) {


        // Setup
        String URL = "jdbc:sqlite::resource:Chinook_Sqlite.sqlite";
        Connection conn = null;
        Connection conn2 = null;
        ArrayList<Customer> customers = new ArrayList<Customer>();

        try {
            // Open Connection
            conn = DriverManager.getConnection(URL);
            System.out.println("Connection to SQLite has been established.");

            // Prepare Statement
            PreparedStatement preparedStatement =
                    conn.prepareStatement("SELECT  CustomerId, FirstName, LastName FROM customer"); //query gets customerid first and last name from customer
            // Execute Statement
            ResultSet resultSet = preparedStatement.executeQuery();

            // Process Results
            while (resultSet.next()) {
                customers.add( //stores what the query downloaded to a arraylist based on the class Customer
                        new Customer(
                                resultSet.getInt("CustomerId"),
                                resultSet.getString("FirstName"),
                                resultSet.getString("LastName")
                        ));
            }

            System.out.println("Choose a number between 1 to " + customers.size() + " or leave type N");
            Scanner scan = new Scanner(System.in);
            int random = (int) (Math.random() * (customers.size()));
            if(scan.hasNextInt()) { //if the user types a int
                int number = Integer.parseInt(scan.nextLine());
                for (Customer c : customers) {
                    if (c.getCustomerID() == number) //chose the customer with the same ID as ui
                        System.out.println(c.getFirstName() + " " + c.getLastName());
                    preparedStatement =                                                     //this query checks for genre.Name, group by genre.Name and then order it by genre.Name so that the most popular genre comes first. Limit 1 is a bad way to only show the first genre name :)
                            conn.prepareStatement("SELECT genre.Name FROM genre, track, invoiceLine, invoice, customer WHERE  genre.GenreId = track.GenreId AND track.TrackId = invoiceLine.TrackId AND invoiceLine.InvoiceId = invoice.InvoiceId AND invoice.CustomerId = customer.CustomerId AND customer.CustomerId = ? GROUP BY genre.Name ORDER BY COUNT(genre.Name) DESC LIMIT 1");
                    preparedStatement.setInt(1, number); //puts the chosen customerID into the query
                }
            } else if (scan.hasNextLine()) { //so basically if the user types anything else than a int. Not a greart solution - i kNOW - but felt like that wasnt the most important thing to fix

                for (Customer c : customers)

                    if (c.getCustomerID() == random) //chose a random character
                        System.out.println(c.getFirstName() + " " + c.getLastName());
                preparedStatement =                                                             //the same query as above
                        conn.prepareStatement("SELECT genre.Name FROM genre, track, invoiceLine, invoice, customer WHERE  genre.GenreId = track.GenreId AND track.TrackId = invoiceLine.TrackId AND invoiceLine.InvoiceId = invoice.InvoiceId AND invoice.CustomerId = customer.CustomerId AND customer.CustomerId = ? GROUP BY genre.Name ORDER BY COUNT(genre.Name) DESC LIMIT 1");
                preparedStatement.setInt(1, random);
            }


                // Execute Statement
                resultSet = preparedStatement.executeQuery();
                ArrayList<Customer> customersGenre = new ArrayList<Customer>(); //creates a arraylist that stores genrename

                // Process Results
                while (resultSet.next()) {
                    customersGenre.add(
                            new Customer(
                                    resultSet.getString("Name") //stores the genre name
                            ));
                }


                for (Customer c : customersGenre) {
                        System.out.println(c.getGenreName()); //and prints the genre name
                }


        } catch (
                Exception ex) {
            System.out.println("Something went wrong...");
            System.out.println(ex.toString());
        } finally {
            try {
                // Close Connection
                conn.close();
            } catch (Exception ex) {
                System.out.println("Something went wrong while closing connection.");
                System.out.println(ex.toString());
            }
        }
    }
}