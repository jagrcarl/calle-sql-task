package se.experis;

public class Customer {

    private int customerID;
   // private String contactName;
    private String city;
    private String phone;
    private String firstName;
    private String lastName;
    private String genreName;

    public Customer() {

    }



    public Customer(int customerID,String firstName, String lastName) {
        this.customerID = customerID;
        this.firstName = firstName;
        this.lastName = lastName;
    }

    public Customer(int customerID, String firstName, String lastName, String genreName) {
        this.customerID = customerID;
        this.firstName = firstName;
        this.lastName = lastName;
        this.genreName = genreName;
    }

    public Customer(String genreName) {
        this.genreName = genreName;


    }

    public String getGenreName() {
        return genreName;
    }

    public void setGenreName(String genreName) {
        this.genreName = genreName;
    }


    public int getCustomerID() {
        return customerID;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String flastName) {
        this.lastName = lastName;
    }

    public void setCustomerID(int customerID) {
        this.customerID = customerID;
    }

   /* public String getContactName() {
        return contactName;
    }

    public void setContactName(String contactName) {
        this.contactName = contactName;
    }
*/
    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }
}

